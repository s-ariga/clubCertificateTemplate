﻿#!/usr/bin/env python
# -*- coding : utf-8 -*-

import datetime

TEMPLATE_DIR = "../template/"
TEMPLATE_INDIVIDUAL = TEMPLATE_DIR + "certificate-indivi.html"
TEMPLATE_MIXTEAM = TEMPLATE_DIR + "certificate-mixteam.html"
TEMPLATE_TEAM_POSI = TEMPLATE_DIR + "certificate-tem-posi.html"
TEMPLATE_TEAM_ALL = TEMPLATE_DIR + "certificate-team-all.html"

def createCerts():
    pass


if __name__ == "__main__":
    createCerts()
